<?php     

namespace ContextualCode\FacebookPostImporterBundle\Services;

use eZ\Publish\Core\Repository\Values\Content\Location;
use eZ\Publish\API\Repository\Values\Content\Field;
use eZ\Publish\API\Repository\Values\Content\Search\SearchHit;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;
use eZ\Publish\API\Repository\Values\Content\Search\SearchResult;
use eZ\Publish\Core\Repository\Values\Content\Content;

/**
 *  Service to create eZ objects from Facebook posts from a page
 *  using a supplied mapping of post edges to callables.
 */
class FacebookPostImporterService
{
   
    protected $contentService;
    protected $contentTypeService;
    protected $locationService;
    protected $searchService;

    protected $fbParams = array(
        'page_id'          => null,
        'app_id'           => null,
        'app_secret'       => null,
        'access_token'     => null,
        'facebook_version' => null
    );

    protected $fb;

    protected $containerNodeID;
    protected $importClassID;
    protected $fbPostIDAttribute;

    protected $lang = 'eng-US';

    protected $callableForEdge = array(
        'none'            => NULL,
        'attachments'     => NULL,
        'comments'        => NULL,
        'insights'        => NULL,
        'likes'           => NULL,
        'private_replies' => NULL,
        'reactions'       => NULL,
        'sharedposts'     => NULL
    );

    public function __construct($locationService, $contentService, $contentTypeService, $searchService, $params)
    {
        $this->locationService    = $locationService;
        $this->contentService     = $contentService;
        $this->contentTypeService = $contentTypeService;
        $this->searchService      = $searchService;

        foreach($this->fbParams as $key => $value) {
            if (isset($params['params'][$key])) {
                $this->fbParams[$key] = $params['params'][$key];
            }
        }        

        $this->initializeFB();
    }  

    public function setFBParam($key, $value)
    {
        $allowedKeys = array_keys($this->fbParams);
        if (!in_array($key, $allowedKeys)) {
            throw new \Exception("Key $key is not valid. Choose one of: " . implode(', ', $allowedKeys));
        }

        $this->fbParams[$key] = $value;

        $this->initializeFB();

        return true;
    }

    protected function initializeFB()
    {
        if (isset($this->fbParams['app_id']) &&
            isset($this->fbParams['app_secret']) &&
            isset($this->fbParams['facebook_version']) &&
            isset($this->fbParams['access_token'])
           ) {
            $this->fb = new \Facebook\Facebook([
              'app_id'                => $this->fbParams['app_id'],
              'app_secret'            => $this->fbParams['app_secret'],
              'default_graph_version' => $this->fbParams['facebook_version'],
              'default_access_token'  => $this->fbParams['access_token']
            ]);

            return true;
        }
           
        return false; 
    }

    public function setLanguage($x)
    {
        $this->lang = $x;
    }

    public function getLanguage($x)
    {
        return $this->lang;
    }

    public function setContainerNodeID($x)
    {
        $this->containerNodeID = $x;
    }

    public function getContainerNodeID($x)
    {
        return $this->containerNodeID;
    }

    public function setImportClassID($x)
    {
        $this->importClassID = $x;
    }

    public function getImportClassID($x)
    {
        return $this->importClassID;
    }

    public function setFBPostIDAttribute($x)
    {
        $this->fbPostIDAttribute = $x;
    }

    public function getFBPostIDAttribute($x)
    {
        return $this->fbPostIDAttribute;
    }

    public function setCallableForEdge($callable, $postEdge = 'none')
    {
        $postEdge = ltrim($postEdge, '/');
        $postEdge = rtrim($postEdge, '/');

        if (!array_key_exists($postEdge, $this->callableForEdge)) {
            return false;
        }

        if (!is_callable($callable)) {
            return false;
        }

        $this->callableForEdge[$postEdge] = $callable; 

        return true;
    }

    public function import($since = null, $until = null, $verbose = false)
    {
        if (!isset($this->fb)) {
            $missingParams = array();
            foreach ($this->fbParams as $key => $param) {
                if (!isset($param)) {
                    $missingParams[] = $key;
                }
            }
            throw new \Exception('FB API is not initialized. Missing params: ' . implode(', ', $missingParams));
        }

        if (!isset($this->importClassID)) {
            throw new \Exception('Import class ID is not initialized.');
        }

        if (!isset($this->containerNodeID)) {
            throw new \Exception('Container node ID is not initialized.');
        }

        if ($verbose) {
            print("Getting FB posts...\r\n");
            if ($since) {
                print("Since: $since\r\n");
            }
            if ($until) {
                print("Until: $until\r\n");
            }
        }

        $fbPosts = $this->getFBPosts($since, $until);

        if ($verbose) {
            print("Got all FB posts (" . count($fbPosts) . " total)...\r\n");
        }

        $alreadyImportedCount = 0;
        $skippedCount = 0;
        $publishedCount = 0;

        $contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->importClassID);

        foreach ($fbPosts as $post) {
            // skip the post if we've already imported it before
            $postID = $post["id"];
            if ($this->isPostAlreadyImported($postID)) {
                $alreadyImportedCount++;
                continue;
            }

            // create object to pass to the callables
            $contentCreateStruct = $this->contentService->newContentCreateStruct($contentType, $this->lang);

            // run each callable with the data and object
            $publish = true;
            foreach ($this->callableForEdge as $edge => $callable) {
                if (!is_callable($callable)) {
                    continue;
                }

                if ($edge == 'none') {
                    $data = $post;
                }
                else {
                    $data = $this->getAllEdges("/{$postID}/{$edge}");
                }

                $params = array($data, &$contentCreateStruct);
                $result = call_user_func_array($callable, $params);
                if (!$result) {
                    $publish = false;
                    $callableName = $callable;
                    if (is_array($callableName)) {
                        $callableName = implode(' ', $callableName);
                    }
                    $skippedCount++;
                    break;
                }
            }
        
            if ($publish) {
                // publish the eZ object
                $locationCreateStruct = $this->locationService->newLocationCreateStruct($this->containerNodeID);
                $draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
                $content = $this->contentService->publishVersion($draft->versionInfo);
                $publishedCount++;

                if ($verbose) {
                    print("Published object for post " . $post["id"] . "...\r\n");
                }
            }
        }

        if ($verbose) {
            print("Skipped already imported: $alreadyImportedCount.\r\n");
            print("Skipped: $skippedCount.\r\n");
            print("Published: $publishedCount.\r\n");
            print("Done.\r\n");
        }

        return true;
    }

    protected function isPostAlreadyImported($postID)
    {
        if (!isset($this->fbPostIDAttribute) || empty($this->fbPostIDAttribute)) {
            return false;
        }

        $criteria = array(
            new Criterion\ParentLocationId($this->containerNodeID),
            new Criterion\ContentTypeIdentifier($this->importClassID),
            new Criterion\Field($this->fbPostIDAttribute, Criterion\Operator::EQ, $postID)
        );

        $query = new LocationQuery(
            array(
                'filter' => new Criterion\LogicalAnd($criteria),
                'limit' => 1
            )
        );

        $results = $this->searchService->findLocations($query);

        return ($results->totalCount > 0);
    }

    protected function getFBPosts($since, $until)
    {
        $params = array(
            'since' => $since,
            'until' => $until
        );
        $endpoint = "/{$this->fbParams['page_id']}/posts?" . http_build_query($params);
        return $this->getAllEdges($endpoint);
    }

    protected function getAllEdges($endpoint) {
        $edges = array();

        try {
            $response = $this->fb->get($endpoint);
        } catch (\Exception $e) {
            return $edges;
        }

        $next = $response->getGraphEdge();
        while ($next) {
            foreach ($next as $edge) {
                $edges[] = $edge->asArray();
            }
            $next = $this->fb->next($next);
        }
        
        return $edges;
    }

} 

